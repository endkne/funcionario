package DAO;

import DTO.FuncionarioDTO;
import Util.FabricaDeConexao;
import static Util.FabricaDeConexao.getConexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author endrigo
 */
public class FuncionarioDAO {

    public FuncionarioDTO retornarPorId(int id) {
        FuncionarioDTO funcionario = null;
        try {
            Connection con = FabricaDeConexao.getConexao();
            String sql = "SELECT * FROM funcionario WHERE id = ?;";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                funcionario = new FuncionarioDTO(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getInt("idade"),
                        rs.getString("telefone")
                );
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return funcionario;

    }

    public ArrayList listaDeFuncionarios() {
        ArrayList<FuncionarioDTO> funcionarios = new ArrayList();
        try {
            Connection con = FabricaDeConexao.getConexao();
            String sql = "SELECT * FROM funcionario;";

            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                funcionarios.add(new FuncionarioDTO(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getInt("idade"),
                        rs.getString("telefone")
                ));
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return funcionarios;
    }

    public ArrayList consulta(String pesquisa) {
        ArrayList<FuncionarioDTO> mensagens = new ArrayList();
        try {
            pesquisa = "%" + pesquisa + "%";
            Connection con = FabricaDeConexao.getConexao();

            String sql = "SELECT * FROM funcionario WHERE nome like ?;";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, pesquisa);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                mensagens.add(new FuncionarioDTO(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getInt("idade"),
                        rs.getString("telefone")
                ));
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mensagens;
    }

    public void inserir(FuncionarioDTO func) {
        try {
            Connection con = getConexao();
            String sql = "INSERT INTO funcionario(nome,idade,telefone) VALUES(?, ?, ?);";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, func.getNome());
            ps.setInt(2, func.getIdade());
            ps.setString(3, func.getTelefone());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizar(FuncionarioDTO func) {
        try {
            Connection con = getConexao();
            String sql = "UPDATE funcionario SET nome = ?, idade = ?, telefone = ? WHERE id = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, func.getNome());
            ps.setInt(2, func.getIdade());
            ps.setString(3, func.getTelefone());
            ps.setInt(4, func.getId());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void excluir(int id){
    
        try {
            Connection con = getConexao();
            String sql = "DELETE FROM funcionario WHERE id = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
