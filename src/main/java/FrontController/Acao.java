package FrontController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author endrigo
 */
public interface Acao {
    String executar(HttpServletRequest request, HttpServletResponse response) throws Exception;
}
