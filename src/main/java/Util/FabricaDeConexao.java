package Util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author endrigo
 */
public class FabricaDeConexao {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/programacaov?serverTimezone=America/Sao_Paulo";

    static final String USER = "root";
    static final String PASS = "bla";

    public static Connection getConexao() {
        Connection conn = null;
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FabricaDeConexao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(FabricaDeConexao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }
}
