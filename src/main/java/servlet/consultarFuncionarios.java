package servlet;

import DAO.FuncionarioDAO;
import FrontController.Acao;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author endrigo
 */
public class consultarFuncionarios implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {
        FuncionarioDAO fDAO = new FuncionarioDAO();
        request.setAttribute("listaDeFuncionarios", fDAO.consulta(request.getParameter("pesquisa")));
        return "listaDeFuncionarios.jsp";
    }
}
