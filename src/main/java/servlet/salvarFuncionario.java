package servlet;

import DAO.FuncionarioDAO;
import DTO.FuncionarioDTO;
import FrontController.Acao;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author endrigo
 */
public class salvarFuncionario implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) {
        FuncionarioDAO fDAO = new FuncionarioDAO();
        FuncionarioDTO funcionario = new FuncionarioDTO(
                Integer.parseInt(request.getParameter("id")),
                request.getParameter("nome"),
                Integer.parseInt(request.getParameter("idade")),
                request.getParameter("telefone"));

        if (funcionario.getId() > 0) {
            fDAO.atualizar(funcionario);
        }else{
            fDAO.inserir(funcionario);
        }
        
        return "index.html";
    }
}
