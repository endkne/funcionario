package servlet;

import DAO.FuncionarioDAO;
import DTO.FuncionarioDTO;
import FrontController.Acao;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author endrigo
 */
public class cadastroFuncionario implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) {
        FuncionarioDAO fDAO = new FuncionarioDAO();

        if (request.getParameter("id") == null || Integer.parseInt(request.getParameter("id")) == 0) {
            request.setAttribute("funcionario", new FuncionarioDTO(0, "", 0, ""));
        } else {
            request.setAttribute("funcionario", fDAO.retornarPorId(Integer.parseInt(request.getParameter("id"))));
        }

        return "cadastrarFuncionario.jsp";
    }
}
