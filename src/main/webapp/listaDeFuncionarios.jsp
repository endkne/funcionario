<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <titleFuncionários</title>
    </head>
    <body>
        <h1>Listar todos</h1>
        <table>
            <td>
                Id
            </td>
            <td>
                Nome
            </td>
            <td>
                Idade
            </td>
            <td>
                Telefone
            </td>
            <c:forEach var="funcionario" items="${listaDeFuncionarios}">
                <tr>
                    <td>
                        <a href="Controller?acao=cadastroFuncionario&id=${funcionario.id}"> ${funcionario.id}    </a>
                    </td>
                    <td>
                        ${funcionario.nome}    
                    </td>
                    <td>
                        ${funcionario.idade}    
                    </td>
                    <td>
                        ${funcionario.telefone}    
                    </td>
                    <td>
                        <a href="Controller?acao=excluirFuncionario&id=${funcionario.id}"> Excluir   </a>
                    </td>
                </tr>
            </c:forEach>   

        </table>
    </body>
</html>
