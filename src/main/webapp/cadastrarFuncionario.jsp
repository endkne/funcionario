<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastrar funcionário</title>
    </head>
    <body>
        <h1>Cadastro de funcionários</h1>
        <form>
            <input name="acao" type="hidden" value="salvarFuncionario">
            <input name="id" type="hidden" value="<c:out value="${funcionario.getId()}" />" />
            <input name="nome" type="text" value="<c:out value="${funcionario.getNome()}" />" />
            <input name="idade" type="text" value="<c:out value="${funcionario.getIdade()}" />" />
            <input name="telefone" type="text" value="<c:out value="${funcionario.getTelefone()}" />" />
            <input type="submit" />
        </form>
    </body>
</html>
